import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../apis/user.service';
import {User} from '../models/user.model';
import {AlertService} from '../services/alert.service';
import {ChangePhoneModalComponent} from '../change-phone-modal/change-phone-modal.component';
import {AuthService} from '../services/auth.service';
import {Personal} from '../models/personal.model';
import {GoogleAuthModalComponent} from '../google-auth-modal/google-auth-modal.component';

declare let intlTelInputUtils: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  @ViewChild('changePhoneModal') changePhoneModal: ChangePhoneModalComponent;
  @ViewChild('googleAuthModal') googleAuthModal: GoogleAuthModalComponent;


  model: any = {
    user: Personal
  };
  ng2TelInputOptions = {
    nationalMode: true,
    utilsScript: '../../build/js/utils.js'
  };
  user: User = {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    zipcode: '',
    country: '',
    city: '',
    state: '',
    address: '',
    birthdate: '',
    address2: '',
    phoneVerified: false,
    mfa: false,
    softwareMfa: false,
    trackingAddresses: [],
    availableMfa: [],

  };
  emailReadonly = '';
  phonePlaceHolder = '';
  states: any[];
  countries: any[];
  intlTelInputHolder: any;

  constructor(
    private alertService: AlertService,
    private authService: AuthService,
    public userService: UserService) {

  }

  telInputObject($event) {
    this.intlTelInputHolder = $event;
  }

  prepareUser() {
    // +19544884203
    this.user = this.userService.currentUser;
    this.model = {user: new Personal(this.user)};
    this.hideEmail();
    this.phonePlaceHolder = this.user.phone.replace(/[+,\d](?=\d{3})/g, 'x');
    this.intlTelInputHolder.intlTelInput('setNumber', this.user.phone);
    this.intlTelInputHolder.intlTelInput('setNumber', this.phonePlaceHolder);

  }

  ngOnInit() {
    this.userService.me().subscribe(r => {
      this.prepareUser();

      this.authService.getCountries().subscribe(
        (data: any) => {
          if (data.success) {
            this.countries = data.data;
            this.onCountrySelect();

          }
        }
      );
    });

    // this.user = JSON.parse(localStorage.getItem('user'));
    // this.emailReadonly = this.user.email.replace(/(\w{1})(\w+)(@.*)(\..*)/ig, '$1***@***$4');
  }


  onCountrySelect() {
    this.states = [];
    this.authService.getStates(this.model.user.country).subscribe(
      (data: any) => {
        if (data.success) {
          this.states = data.data;
        }
      }
    );
  }

  showEmail() {
    this.emailReadonly = this.user.email;
  }

  hideEmail() {
    this.emailReadonly = this.user.email
      .replace(/(\w)(\w+)(@.*)(\..*)/ig, '$1***@***$4');
  }

  changeEmail(nForm) {

    if (!this.model.email) {
      this.alertService.error('Missing email address!');
      return;
    } else if (!nForm.valid) {
      this.alertService.error('Invalid email address!');
      return;
    }
    this.userService.updateEmail(this.model.email)
      .subscribe((r: any) => {
        if (r.success) {
          this.alertService.success('Email Address updated successful.');
          this.prepareUser();
        } else {
          this.alertService.error(r.error[0].message);
        }
      }, error => {
        this.alertService.error(error.statusText);
      });
    {

    }
  }


  changePassword(nForm) {

    if (!this.model.oldPassword) {
      this.alertService.error('Missing old password!');
      return;
    } else if (!this.model.newPassword || this.model.newPassword.length < 6) {
      this.alertService.error('Missing new password or less the 6 chars!');
      return;
    } else if (!this.model.confirmPassword || this.model.newPassword.length < 6) {
      this.alertService.error('Missing confirm new password or less the 6 chars!');
      return;
    } else if (this.model.confirmPassword !== this.model.newPassword) {
      this.alertService.error('New password and confirm new password does not match!');
      return;
    }

    this.userService.updatePassword(this.model.oldPassword, this.model.newPassword)
      .subscribe(r => {
        if (r.success) {
          this.prepareUser();
          this.alertService.success('Password updated successful.');
        } else {
          this.alertService.error(r.error[0].message);
        }
      }, error => {
        console.log(error);
        this.alertService.error((error.error && error.error.message) ?
          error.error.message
          : error.statusText);
      });

  }

  changePhone(resend = false) {
    this.changePhoneModal.openModal(resend)
      .subscribe(r => {
        this.userService.me()
          .subscribe(r2 => {
            this.prepareUser();
          });
      });
  }

  googleAuth() {
    this.googleAuthModal.openModal()
      .subscribe(r => {
        this.userService.me()
          .subscribe(r2 => {
            this.prepareUser();
          });
      });
  }


  changePersonalInformation(nForm) {
    if (nForm.valid) {
      this.userService.update(this.model.user.getChanges())
        .subscribe((r: any) => {
          if (r.success) {
            this.alertService.success('Your personal information updated successful.');
            this.prepareUser();
          } else {
            this.alertService.error(r['error'][0]['message']);
          }
        });
    } else {
      this.alertService.error('Invalid form value!');
    }
  }

}
