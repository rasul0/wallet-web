import { Component, OnInit, OnDestroy, Renderer2, ViewChild,NgModule, ModuleWithProviders,
         Output, Input , Directive , ElementRef , EventEmitter, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import {AuthService} from '../services/auth.service';
import {Register} from '../models/register.model';
import { Ng2TelInput } from 'ng2-tel-input';
import * as $ from 'jquery';

declare let intlTelInputUtils:any;

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
	@ViewChild('registerForm') registerForm;
	@ViewChild ('verificationForm') verificationForm;

	public repassword = '';
	register: Register = {
		firstName: '',
		lastName: '',
		country: 'CA',
		address:'',
		city:'',
		state:'',
		nin:'',
		email:'',
		phone:'',
		birthdate:'',
		username: '',
		password: '',
		zipcode: ''
	};
	countries=[];
	states=[];
	tempDate: '';
	error: string = '';
	isSubmitted: boolean = false;
	checkboxValue:boolean;
	showCodeConfirmation:boolean= false;
	code : any ='';

	constructor(
		private http: HttpClient,
		private authService: AuthService,
		private router: Router,
		private renderer: Renderer2
		) {
		this.renderer.addClass(document.body, 'register');
	}
	
	submit(registerInfo: any, isValid: boolean, registerForm){
		this.telInput();
	    let temp = 'Please Enter Valid ';
	    if (!isValid) {
	    	if (this.register.firstName == '') {
	    		temp += 'First Name ';
	    	}
	    	if (this.register.lastName == '') {
	    		temp += 'Last Name ';
	    	}
	    	if (this.register.country == '') {
	    		temp += 'Country ';
	    	}
	    	if (this.register.address == '') {
	    		temp += 'Address ';
	    	}
	    	if (this.register.city == '') {
	    		temp += 'City ';
	    	}
	    	if (this.register.state == '') {
	    		temp += 'State ';
	    	}
	    	if (this.register.nin == '') {
	    		temp += 'NIN ';
	    	}
	    	if (this.register.email == '') {
	    		temp += 'Email ';
	    	}
	    	if (this.register.phone == '') {
	    		temp += 'Phone Number  ';
	    	}
	    	if (this.register.birthdate == '') {
	    		temp += 'Birth Date ';
	    	}
	    	if (this.register.username == '') {
	    		temp += 'Username ';
	    	}
	    	if (this.register.password == '') {
	    		temp += 'Password ';
	    	}
	    	if (this.register.zipcode == '') {
	    		temp += 'Zip Code ';
	    	}
	    	//this.sweetAlert(temp);


	    } else {
	    	temp = '';
	    	let date = new Date(this.tempDate);
	    	let newDate = date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear()
	    	this.register.birthdate = newDate;


	    	this.error = '';
	    	this.isSubmitted = false;
	    	if (isValid) {
	    		if (registerInfo.password != registerInfo.repassword) {
	    			this.sweetAlert( 'Password & Re-passwrod should match');
	    			
	    		} else if (!this.validateEmail(registerInfo.email)) {
	    			this.sweetAlert('Please enter a valid email');
	    		} else if(!this.checkboxValue){
	    		    this.sweetAlert('Please accept terms and condtitions');
	    		}
	    		else {
	    			this.isSubmitted = true;
	    			this.authService.register(this.register).subscribe(
	    				(data:any) => {
	    					this.error = '';
	    					this.isSubmitted = false;
	    					if(data.success) {
	    						if(data.data.signupConfirmation){
	    							swal('Success','The code is sent ','success');
	    							this.showCodeConfirmation=true;
	    						}
	    						else {
	    							swal('Success','Signup Successful! Now Please Login ','success');
	    							this.router.navigate(['login']);
	    						}
	    					} else {
	    						data.error.forEach((e) => {
	    							this.error += e.message + '\n';
	    						});
	    						this.sweetAlert(this.error);
	    						this.error='';
	    					}
	    				},
	    				(error: any) => {
	    					this.error = '';
	    					this.isSubmitted = false;
	    					this.error = error.error.message;
	    					this.sweetAlert(this.error);
	    					this.error='';
	    				}
	    				);
	    		} 
	    	}
	    } 
	}

	sendCode(verificationInfo: any, isValid: boolean, verificationForm){
		if(!isValid) {
			if(this.code == '') {
				//this.sweetAlert('Please Enter Code');
			}
		}
		if (isValid) {
			this.isSubmitted=true;
			this.authService.isConfirmationSuccess(this.register.username,this.code).subscribe(
				(data:any) => {
					this.isSubmitted =false;
					if(data.success){
						swal('Success','Signup Successful! Now Please Login ','success');
						this.router.navigate(['login']);
					}
					else {
						data.error.forEach((e) => {
							this.error += e.message + '\n';
						});
						this.sweetAlert(this.error);
	    				this.error='';
					}
			});
		}
	}

	sweetAlert(msg : string) {
		swal("Warning",msg,'warning');
	}


	ngOnInit() {
		this.authService.getCountries().subscribe(
			(data:any)=>{
				console.log(data);
				if (data.success) {
					this.countries = data.data;
					this.onCountrySelect();

				}
			}
		);
	}

	ngOnDestroy() {
		this.renderer.removeClass(document.body, 'register');
	}

	validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	onCountrySelect(){
		this.states=[];
		this.authService.getStates(this.register.country).subscribe(
			(data:any)=>{
				if (data.success) {
					this.states = data.data;
				}
			}
		);
	}

	telInput(){
		let phonenum = $("#phoneNumber").intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);
		this.register.phone = phonenum;
	}
}
