import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {AlertModule, BsDropdownModule, CollapseModule, ModalModule, TooltipModule} from 'ngx-bootstrap';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {DashboardComponent} from './dashboard/dashboard.component';

import {AuthInterceptor} from './services/auth.interceptor';
import {AuthService} from './services/auth.service';
import {APIsModule} from './apis/apis.module';
import {DashboardResolver} from './dashboard/dashboard.resolver';
import {Ng2TelInputModule} from 'ng2-tel-input';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {InViewportModule} from 'ng-in-viewport';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


import {SendModalComponent} from './send-modal/send-modal.component';
import {AlertService} from './services/alert.service';
import {ReceiveModalComponent} from './receive-modal/receive-modal.component';
import {ClipboardModule} from 'ngx-clipboard';
import {NgxQRCodeModule} from 'ngx-qrcode2';
import {UpperMenuComponent} from './upper-menu/upper-menu.component';
import {ProfileComponent} from './profile/profile.component';
import {ChangePhoneModalComponent} from './change-phone-modal/change-phone-modal.component';
import {NgxMaskModule} from 'ngx-mask';
import {GoogleAuthModalComponent} from './google-auth-modal/google-auth-modal.component';
import {TextMaskModule} from 'angular2-text-mask';
import { ChartComponent } from './chart/chart.component';




const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    ForgotPasswordComponent,
    SendModalComponent,
    ReceiveModalComponent,
    UpperMenuComponent,
    ProfileComponent,
    ChangePhoneModalComponent,
    GoogleAuthModalComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    APIsModule,
    Ng2TelInputModule,
    InViewportModule,
    ClipboardModule,
    NgxQRCodeModule,
    AlertModule.forRoot(),
    PerfectScrollbarModule,
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    NgxMaskModule.forRoot(),
    TextMaskModule,
    TooltipModule.forRoot()
  ],
  providers: [

    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,

    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    DashboardResolver,
    AlertService,
    DashboardResolver,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
