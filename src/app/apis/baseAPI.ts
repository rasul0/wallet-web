/**
 * Created by iAboShosha on 7/13/17.
 */
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';


@Injectable()
export class API {
  protected baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  /*
  * Get http method
  * reruns array of the Type T
  * inputs apiUrl part of the url after the base url
  * qs : query string
  * */
  get<T>(apiUrl: string, qs?: string): Observable<T[]> {
    return this.http
      .get(`${this.baseUrl}/${apiUrl}${qs ? '?' + qs : ''}`)
      .pipe(
        map(res => {
          if (res && res['success'] === false) {
            throw new Error(res['error'][0]['message']);
          }
          return (res as { data }).data;
        }),
        catchError((err: HttpErrorResponse) => {
          throw new Error(err.statusText);
        })
      );
  }

  /*
  * Get http method
  * reruns one object of Type T
  * inputs apiUrl part of the url after the base url
  * qs : query string
  * */
  getOne<T>(apiUrl: string, qs?: string): Observable<T> {
    return this.http
      .get(`${this.baseUrl}/${apiUrl}${qs ? '?' + qs : ''}`)
      .pipe(
        map(res => {
          if (res && res['success'] === false) {
            throw new Error(res['error'][0]['message']);
          }
          return (res as { data }).data;
        }),
        catchError((err: HttpErrorResponse) => {
          throw new Error(err.statusText);
        })
      );
  }

  /*
  * POST http method
  * reruns one object of Type T
  * inputs apiUrl part of the url after the base url
  * data : data that will be send throw the body
  * */
  post<T>(apiUrl: string, data: any) {
    return this.http
      .post<T>(`${this.baseUrl}/${apiUrl}`, data);
  }

  /*
   * PUT http method
   * reruns one object of Type T
   * inputs apiUrl part of the url after the base url
   * data : data that will be send throw the body
   * */
  put<T>(apiUrl: string, data: any) {
    return this.http
      .put<T>(`${this.baseUrl}/${apiUrl}`, data);
  }

  /*
 * onTimeInterval event
 * to be implemented in the sub classes
 * */
  protected onTimeInterval(): Subject<Boolean> | Observable<Boolean> {
    return Observable.create(true);
  }

  /*
  * time interval service
  * it will be executed basedon the environment setting `interval`*/
  startInterval() {
    const that = this;
    return setTimeout(function () {
      that.onTimeInterval().subscribe(_continue => {
        if (_continue) {
          that.startInterval();
        } else {
          console.error('timer stop :: ', _continue);
        }
      }, err => {
        that.startInterval();
        console.error('timer stop :: ', err);
      });
    }, environment.interval);
  }

  checkForErrors(x: Observable<any>): Observable<boolean> {
    return new Observable<boolean>(observer => {
      const h = x.subscribe(
        c => {
          observer.next(true);
          observer.complete();

        },
        error1 => {
          console.error('errror', error1);
          observer.next(error1.status < 400 || error1.status >= 500);
          observer.complete();
        }, () => h.unsubscribe());
    });
  }

}

