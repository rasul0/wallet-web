import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {TransactionService} from './transaction.service';
import {SymbolService} from './symbol.service';
import {WalletService} from './wallet.service';
import {UserService} from './user.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    WalletService,
    TransactionService,
    SymbolService,
    UserService
  ],
  declarations: []
})
export class APIsModule {
}
