import {API} from './baseAPI';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user.model';
import {tap} from 'rxjs/operators';


@Injectable()
export class UserService extends API {

  constructor(http: HttpClient) {
    super(http);
    this._user = JSON.parse(localStorage.getItem('user')) as User;
  }

  _user: User;

  me() {
    return this.getOne<User>('user')
      .pipe(tap((r: any) => {
        this.setCurrentUser(r);
      }));
  }

  public get currentUser(): User {
    return this._user;
  }

  private setCurrentUser(u: User) {
    localStorage.setItem('user', JSON.stringify(u));
    this._user = u;
  }

  update(obj) {
    return this.put('user/update', obj)
      .pipe(tap((r: any) => {
        if (r.success) {
          this.setCurrentUser(r.data);
        }
      }));
  }

  updateEmail(email: string) {
    return this.update({email});
  }

  updatePassword(oldPassword, newPassword) {
    return this.update({oldPassword, newPassword});
  }

  verifyPhone(code) {
    return this.post('user/verify/phone', {code});
  }

  verifyEmail(code) {
    return this.post('user/verify/Email', {code});
  }

  softwareToken() {
    return this.post('user/softwareToken', null);
  }

  softwareTokenVerify(code) {
    return this.post('user/softwareToken/verify', {code});
  }
}
