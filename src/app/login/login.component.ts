import { Component, OnInit, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from '../services/auth.service'
import {Login} from '../models/login.model'
import swal from 'sweetalert';


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
	@ViewChild('loginForm') loginForm;
	login: Login = {
		username: '',
		password: '',
	};
	error: string = '';
	isSubmitted: boolean = false;
	checkboxValue:boolean;
	twoFactorAuth:boolean = false;
	code:string;
	session: string;
	destination : string = '';
    medium : string = '';

	constructor(
		private authService: AuthService, 
		private router: Router,
		private renderer: Renderer2
		) {
		this.renderer.addClass(document.body, 'login');
	}

	submit(loginInfo: Login, isValid: boolean, loginForm){
		let temp = 'Please Enter ';
		if(!isValid) {
			if(this.login.username== '') {
				temp += 'Username '
			}
			if(this.login.password == '') {
				temp += 'Password '
			}
			//this.sweetAlert(temp)
		} else {
			this.error = '';
			this.isSubmitted = false;
			if (isValid) {
				this.isSubmitted = true;
				this.authService.login(loginInfo).subscribe(
					(data:any) => {
						this.error = '';
						this.isSubmitted = false;
						if(data.success && !data.data.twoFactorAuth) {
							localStorage.setItem('accessToken', data.data.accessToken);
							localStorage.setItem('user', JSON.stringify(data.data.user));
							this.router.navigate(['dashboard']);
						} else if (data.success && data.data.twoFactorAuth) {
							localStorage.setItem('session', data.data.session);
							this.session = data.data.session;
							this.login.username= data.data.twoFactorData.USER_ID_FOR_SRP;
							this.destination= data.data.twoFactorData.CODE_DELIVERY_DESTINATION;
							this.medium=data.data.twoFactorData.CODE_DELIVERY_DELIVERY_MEDIUM;
						    this.twoFactorAuth=true;
						    swal('Success','The code is sent ','success');
					} else {
						data.error.forEach((e) => {
							this.error += e.message + '\n';

						});
						this.sweetAlert(this.error);
						this.error = '';
					}
				},
				(error: any) => {
					this.error = '';
					this.isSubmitted = false;
					this.error = error.error.message;
					this.sweetAlert(this.error);
				}
				);
			}
		}		
	}

	sendCode(verificationInfo: any, isValid: boolean, verificationForm){
		let temp = 'Please Enter ';
		if (!isValid) {
			if(this.code == ''){
				temp += 'Code '
			}
		}
		else {
			this.isSubmitted=true;
			this.authService.loginCode(this.login.username,this.session,this.code).subscribe(
				(data:any) => {
					if(data.success){
						localStorage.setItem('accessToken', data.data.accessToken);
						localStorage.setItem('user', JSON.stringify(data.data.user));
						swal('Success','Welcome ','success');
						this.isSubmitted=false;		
						this.router.navigate(['dashboard']);
					} else {
						data.error.forEach((e) => {
							this.error += e.message + '\n';
						});
						swal('Warning',this.error , 'warning');
						this.error = '';
						this.isSubmitted=false;	
					} 
					this.isSubmitted=false;
				},
				(error: any) => {
					this.error = '';
					this.error = error.error.message;
					this.sweetAlert(this.error);
					this.isSubmitted = false;
				}
				);
		}
	}

	sweetAlert(msg : string) {
		swal("Warning",msg,'warning');
	}

	ngOnInit() {
	}

	ngOnDestroy() {
		this.renderer.removeClass(document.body, 'login');
	}
}
