import {Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ChartComponent implements OnInit {

  @Input()
  data: any;

  constructor() {
  }

  @ViewChild('svg')
  svg: ElementRef;


  createTxtBg(svg, txt) {

    const rect = svg.insert('rect');
    const text = svg.insert('text')
      .text(txt);
    let bbox: any = {};
    //
    text.call(function (sel) {
      sel.each(function (d) {
        bbox = this.getBBox();
      });
    });

    const paddingLeftRight = 18; // adjust the padding values depending on font and font size
    const paddingTopBottom = 6;

    rect
      .attr('x', bbox.x - (paddingLeftRight / 2))
      .attr('y', bbox.y - (paddingTopBottom / 2))
      .attr('width', bbox.width + paddingLeftRight)
      .attr('height', bbox.height + paddingTopBottom)
      .attr('rx', 5)
      .attr('ry', 5)
    ;
  }

  ngOnInit() {
    const price = this.data.marketData.history.price;
    const volume = this.data.marketData.history.volume;
    const svg = d3.select(this.svg.nativeElement);

    // svg.attr('preserveAspectRatio', 'xMinYMin');
    console.log(svg.node().getBoundingClientRect());
    const margin = {top: 10, right: 130, bottom: 30, left: 0},
      width = svg.node().getBoundingClientRect().width,
      height = width * .4; // svg.node().getBoundingClientRect().height;


// set the ranges
    const xBar = d3.scaleBand().range([margin.left, width - margin.right])
      .paddingInner(0.5).paddingOuter(0.25);
    const yBar = d3.scaleLinear().range([0, height - margin.bottom]);

    const xLine = d3.scaleTime().range([0, width - margin.right]);
    const yLine = d3.scaleLinear().range([0, height - margin.bottom]);

// define the 1st line
    const valueline = d3.line()
      .x(function (d) {
        return xLine(d.date);
      })
      .y(function (d) {
        return yLine(d.amount);
      });


    svg.attr('width', width)
      .attr('height', height)
      .append('g')
      .attr('ibrahim', 'oman')
      .attr('transform',
        'translate(' + margin.left + ',' + margin.top + ')');

// Get the data
    // format the data
    volume.forEach(function (d) {
      d.amount = d.amount * 1;
      d.date = new Date(d.date);
    });
    price.forEach(function (d) {
      d.amount = d.amount * 1;
      d.date = new Date(d.date);
    });

    // Scale the range of the data
    xBar.domain(price.map(function (d) {
      return d.date;
    }));

    xLine.domain([
      d3.min(volume, function (d) {
        return d.date;
      }),
      d3.max(volume, function (d) {
        return d.date;
      })]);

    yBar.domain([
      d3.min(price, function (d) {
        return d.amount;
      }),
      d3.max(price, function (d) {
        return d.amount;
      })]).nice();

    yLine.domain([
      d3.min(volume, function (d) {
        return d.amount;
      }),
      d3.max(volume, function (d) {
        return d.amount;
      })]).nice();

    // Add the valueline path.


    // // Add the valueline2 path.
    // svg.append('path')
    //   .data([volume])
    //   .attr('class', 'line')
    //   .style('stroke', 'crimson')
    //   .attr('d', valueline2);

    const rect = svg.selectAll('rect')
      .data(price);

    rect.enter().append('rect')
      .merge(rect)
      .attr('class', 'bar')
      .attr('x', function (d) {
        return xBar(d.date);
      })
      .attr('width', function (d) {
        return xBar.bandwidth();
      })
      .attr('y', function (d) {
        return height - yBar(d.amount) - margin.bottom;
      })
      .attr('height', function (d) {
        return yBar(d.amount);
      })
      .on('mouseover', (d, i, c) => {
        d3.select(c[i])
          .classed('hover', true);
      })
      .on('mouseout', (d, i, c) => {
        d3.select(c[i])
          .classed('hover', false);
      });

    svg.append('path')
      .data([volume])
      .attr('class', 'line')
      // .style('stroke', 'steelblue')
      .attr('d', valueline);

    const points1 = svg.selectAll('circle.point1')
      .data(volume);

    points1.enter().append('circle')
      .merge(points1)
      .attr('class', 'point1')
      // .style('stroke', 'steelblue')
      // .style('fill', 'steelblue')
      .attr('cx', function (d) {
        return xLine(d.date);
      })
      .attr('cy', function (d) {
        return yLine(d.amount);
      })
      .attr('r', function (d) {
        return 2.7;
      })
      .on('mouseover', (d, i, c) => {
          console.log(c[i]);
          d3.select(c[i])
            .classed('hover', true)
            .attr('r', 5);
          svg.append('line')
            .classed('tracker', true)
            .attr('x1', d3.select(c[i]).attr('cx'))
            .attr('y2', margin.top)
            .attr('x2', d3.select(c[i]).attr('cx'))
            .attr('y1', height - margin.bottom)
            .attr('stroke-dasharray', '10, 5');

          let gt = svg.append('g')
            .classed('tracker', true)
            .classed('amount', true)
            .attr('transform', 'translate(' + d3.select(c[i]).attr('cx') + ',' + (margin.top * 2) + ')');
          this.createTxtBg(gt, '$' + d.amount);

          gt = svg.append('g')
            .classed('tracker', true)
            .classed('date', true)
            .attr('transform', 'translate(' + d3.select(c[i]).attr('cx') + ',' + (height - (margin.bottom / 2)) + ')');

          this.createTxtBg(gt, d3.timeFormat('%H:%M')(d.date));
        }
      )
      .on('mouseout', (d, i, c) => {
        d3.selectAll('.tracker').remove();
        d3.select(c[i])
          .classed('hover', false)
          .attr('r', 2.7);
      });


    // Add the X Axis
    let g = svg.append('g')
      .attr('transform', 'translate(0,' + (height - margin.bottom) + ')')
      .call(d3.axisBottom(xLine)
        .ticks(5)
        .tickFormat(d3.timeFormat('%d %b'))
      );

    g.selectAll('.tick line')
      .attr('y1', -height)
      .attr('y2', 0);


    // Add the Y1 Axis
    g = svg.append('g')
      .attr('class', 'axisRed')
      .attr('transform', 'translate( ' + (width - margin.left - margin.right) + ', 0 )')
      .call(
        d3.axisRight(yLine)
          .ticks(4)
          .tickFormat(d3.format('$'))
          .tickPadding(10)
      );


    g.selectAll('.tick line')
      .attr('x1', -(width - margin.left - margin.right))
      .attr('x2', (width - margin.left - margin.right))
    ;

    g.selectAll('.tick text')
      .attr('x', 25)
      .attr('dy', '-0.32em');


    // console.log('ddd', d3.axisLeft(yBar), d3.axisLeft(yLine));


  }

}
