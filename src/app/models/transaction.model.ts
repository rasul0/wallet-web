export class Transaction {

  constructor() {
  }

  public static icons = {
    Receive: 'deposit.png',
    Refund: 'refund.png',
    'Exchange Deposit': 'exchange.png',
    ExchangeReceive: 'exchange.png',
    'Exchange Withdraw': 'sent.png',
    ExchangeSent: 'sent.png',
    Sent: 'sent.png',
    Send: 'sent.png',
    Withdraw: 'sent.png',
    Fee: 'fee.png',
    Purchase: 'payment.png'
  };


}
