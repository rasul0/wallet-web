export interface User {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  zipcode: string;
  address: string;
  address2: string;
  birthdate: string;
  state: string;
  country: string;
  city: string;
  availableMfa: string[];
  mfa: boolean;
  softwareMfa: boolean;
  phoneVerified: boolean;
  trackingAddresses: string[];
}
