import { Component, OnInit, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from '../services/auth.service';
import swal from 'sweetalert';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
	@ViewChild('forgetPasswordForm') forgetPasswordForm ;

	
		code : string = '';
		username : string = '';
		password : string = '';
		reset : boolean =false;
		isSend :boolean = false;
		destination : string = '';
    medium : string = '';
		error : string = '';

  constructor(private authService: AuthService,
  	          private router : Router,
  	) { }

  sendCode(formInfo : string, isValid: boolean, forgetPasswordForm){
  	let temp : string = '';
  	if(!isValid) {
  		if(this.code == '')	{
  			temp += 'Code ' ;
  		}  
  		if (this.password == '') {
  			temp += 'Password ';
  		}
  		//swal("Some Fields are Required!", "Please Enter Valid " + temp , "warning");

  	} else {
      this.isSend=true;
  		this.authService.passwordUpdated(this.username,this.code,this.password).subscribe(
  		(data:any) => {
  			if(data.success){
  				this.isSend=false;
          swal('Success','Password Updated','success');
  				this.router.navigate(['login']);
  			} else {
  				  data.error.forEach((e) => {
  					this.error += e.message + '\n';
  				}); 
  				swal('Warning',this.error , 'warning');
  				this.error = '';
          this.isSend=false;
  			}
  		},
  		(error: any) => {
  			this.error = '';
  			this.error = error.error.message;
  			swal('Warning',this.error,'warning');
        this.isSend = false;
  		});
  	}
  }

  resetPassword(formInfo : string, isValid: boolean, forgetPasswordForm){
  	if(!isValid) {
  		if(this.username == '')	{
  			//swal("Username Required!", "Please Enter Valid Username ", "warning");
  		}
  	}
  	else { 
      this.isSend=true;
  		this.authService.password(this.username).subscribe(
  		(data:any) => {
  			if (data.success) {
  				this.destination = data.data.codeDestination;
          this.medium = data.data.codeDeliveryMethod;
  				this.reset = true;
          swal('Success','The code is sent ','success');
          this.isSend=false;
  			} else {
  				data.error.forEach((e) => {
  					this.error += e.message + '\n';
  				});
          this.isSend=false;
  				swal('Warning',this.error , 'warning');
  				this.error = '';
  			}
  		},
  		(error: any) => {
  			this.error = '';
  			this.isSend = false;
  			this.error = error.error.message;
  			swal('Warning',this.error,'warning');
  		});
  	}
  }

  sweetAlert(msg : string) {
  	swal("Warning",msg,'warning');
  }

  ngOnInit() {
  }

}
