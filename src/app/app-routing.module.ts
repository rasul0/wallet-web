import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuardService} from './services/auth-guard.service';


import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DashboardResolver} from './dashboard/dashboard.resolver';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {ProfileComponent} from './profile/profile.component';

const routes: Routes = [
    {path: '', redirectTo: '/login', pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: '', redirectTo: '/login', pathMatch: 'full'},
    {path: 'forgotpassword', component: ForgotPasswordComponent},
    {path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService]},
    {
      path: 'dashboard',
      component: DashboardComponent,
      resolve:
        {
          defaults: DashboardResolver
        },
      canActivate: [AuthGuardService]
    },
  ]
;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
