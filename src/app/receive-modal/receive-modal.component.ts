import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Wallet} from '../models/wallet.model';
import {BsModalRef} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {WalletService} from '../apis/wallet.service';
import {Symbol} from '../models/symbol.model';
import {Address} from '../models/address.model';
import {AlertService} from '../services/alert.service';

@Component({
  selector: 'app-receive-modal',
  templateUrl: './receive-modal.component.html',
  styleUrls: ['./receive-modal.component.css']
})
export class ReceiveModalComponent implements OnInit {


  @Input()
  selectedWalletData: Wallet;
  @Input()
  selectedSymbolData: Symbol;
  @ViewChild('template')
  template;

  modalRef: BsModalRef;
  address: Address = {
    address: '',
    extra: [],
    symbol: ''
  };

  constructor(
    private modalService: BsModalService,
    private alertService: AlertService,
    public walletService: WalletService) {
  }

  ngOnInit() {
  }

  printElem(divId) {
    const content = document.getElementById(divId).innerHTML;
    const mywindow = window.open('', 'Print', 'height=600,width=800');

    mywindow.document.write('<html><head><title>Print</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write(content);
    mywindow.document.write('</body></html>');

    mywindow.document.close();
    mywindow.focus();
    mywindow.print();
    mywindow.close();
    return true;
  }

  print() {
    this.printElem('receive-modal-body');
  }

  openModal() {
    const that = this;
    this.modalService.config.ignoreBackdropClick = true;
    this.walletService.address(this.selectedWalletData.symbol)
      .subscribe(r => {
        that.address = r;
        that.modalRef = this.modalService.show(that.template);
        that.modalService.onShow.subscribe(c => {
          that.walletService.address(that.selectedWalletData.symbol);
        });
      }, error1 => {
        this.alertService.push('error', error1.message);
      });
  }

}
