import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
/*import { JwtHelperService } from '@auth0/angular-jwt';*/
import {Register} from '../models/register.model'
import {Login} from '../models/login.model'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
	//API_BASEURL = "https://sandbox.spendsdk.com/api/v3/";
	//API_BASELOC = "http://sandbox.spendsdk.com/api/location/";
	API_BASEURL="https://sandbox.spendsdk.com/api";
	//API_BASEURL="http://usnvspd1.spendsdk.com/api";

	user:Register;

   	constructor(private http: HttpClient,private myRoute: Router) {
    }

	register (user: Register){
		return this.http.post(this.API_BASEURL + "/v3/user/signup", JSON.stringify(user));
	}

	login(user:Login){
		return this.http.post(this.API_BASEURL + "/v3/user/login", JSON.stringify(user));
	}

	getCountries(){
		return this.http.get(this.API_BASEURL + "/location/countries");
	}

	getStates(country:string) {
		return this.http.get(this.API_BASEURL + "/location/states/" + country);
	}
	logout(){
   	  return this.http.get(this.API_BASEURL + "/user/logout")
  }

	loginCode(username : string , session : string , code : string){
		const obj = {
			session,
			username,
			code
		}
		return this.http.post(this.API_BASEURL + "/v3/user/login/code",obj);
	}

	isConfirmationSuccess(username:string,code:string){
	    const obj = {
	    	username,
	    	code,
	    }
		return this.http.post(this.API_BASEURL + "/user/signup/verify",obj);

	}

	password(username : string ) {
		const obj = {
			username,
		}
		return this.http.post(this.API_BASEURL + '/v3/user/password',obj)
	}

	passwordUpdated(username:string , code : string , password : string){
		const obj ={
			username ,
			code,
			password,
		}
		return this.http.post(this.API_BASEURL + '/v3/user/password/confirm',obj)
	}

	oneStepSignup (username,email ,password ,phone,firstname,lastname) {
		const obj = {

		}
		return this.http.post(this.API_BASEURL + '/api/v3/user/signup/step1', obj );
	}

	public isAuthenticated(): boolean {
        if(localStorage.accessToken) {
        	return true;
        }
        return false;
    }
}
