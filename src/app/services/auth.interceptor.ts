import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(request.url.indexOf('login') > 0 ||
      request.url.indexOf('countries') > 0 ||
      request.url.indexOf('states') > 0 ||
    request.url.indexOf('signup') > 0 ){
      request = request.clone({
        // withCredentials: true,
        setHeaders: {
          'Accept': 'application/json, text/javascript, */*; q=0.01',
          'Content-Type' : 'application/json'
        }
      });
    } else{
      request = request.clone({
        // withCredentials: true,
        setHeaders: {
          'Accept': 'application/json, text/javascript, */*; q=0.01',
          'Content-Type' : 'application/json',
          'Authorization' : `${this.getAuthToken()}`
        }
      });
    }

    return next.handle(request); //.do((event:HttpEvent<any>) => {
    //   if(event instanceof HttpResponse) {
    //     // log that event successfully intercepted
    //   }
    // },
    // (err: any) => {
    //   if(err instanceof HttpErrorResponse) {
    //     if(err.status === 401) {
    //       // remove token from localStorage and do proper sign out.
    //       // redirect user to login page.
    //     }
    //   }
    // });
  }

  // private getCookie(name: string) {
  //   let ca: Array<string> = document.cookie.split(';');
  //   let caLen: number = ca.length;
  //   let cookieName = `${name}=`;
  //   let c: string;

  //   for (let i: number = 0; i < caLen; i += 1) {
  //     c = ca[i].replace(/^\s+/g, '');
  //     if (c.indexOf(cookieName) == 0) {
  //       return c.substring(cookieName.length, c.length);
  //     }
  //   }
  //   return '';
  // }

  private getAuthToken() {
    return localStorage.accessToken;
  }
}
