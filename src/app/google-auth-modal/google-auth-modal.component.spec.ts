import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleAuthModalComponent } from './google-auth-modal.component';

describe('GoogleAuthModalComponent', () => {
  let component: GoogleAuthModalComponent;
  let fixture: ComponentFixture<GoogleAuthModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleAuthModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleAuthModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
