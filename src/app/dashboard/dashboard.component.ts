import {Component, OnInit, Renderer2, TemplateRef, ViewChild} from '@angular/core';
import {WalletService} from '../apis/wallet.service';
import {SymbolService} from '../apis/symbol.service';
import {ActivatedRoute} from '@angular/router';
import {TransactionService} from '../apis/transaction.service';
import {Wallet} from '../models/wallet.model';
import {Symbol} from '../models/symbol.model';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs';
import * as _ from 'lodash';
import {Transaction} from '../models/transaction.model';
import {SlideInOutAnimation} from '../helper/animations';
import {SendModalComponent} from '../send-modal/send-modal.component';
import {AlertService} from '../services/alert.service';
import {ReceiveModalComponent} from '../receive-modal/receive-modal.component';
import {BsModalService} from 'ngx-bootstrap/modal';
import {BsModalRef} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as $ from 'jquery';
import {Chart} from 'chart.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [SlideInOutAnimation]
})
export class DashboardComponent implements OnInit {
  @ViewChild('sendModal') sendModal: SendModalComponent;
  @ViewChild('receiveModal') receiveModal: ReceiveModalComponent;
  public toggle = false;
  close = true;
  icons = Transaction.icons;
  SearchBar = false;
  searchValue = '';
  menu = false;
  symbolsDisplayed = [];
  modalRef: BsModalRef;
  activeTimeFilter = 0;
  selectedWallet: string;
  selectedTransactionId: string;
  wallets: Wallet[] = [];
  symbols: Symbol[] = [];
  txUrls: any;
  pages = 2;
  page = 1;
  loading = false;
  transactionArray = [];


  selectedWalletData: Wallet = {
    title: '',
    image: '',
    color: '',
    symbol: '',
    balance: null,
    balanceFormatted: '',
    balanceCurrency: null,
    balanceCurrencyFormatted: '',
    isActive: null,
    isDefault: false
  };
  selectedSymbolData: Symbol = {
    title: '',
    symbol: '',
    card: '',
    image: '',
    status: null,
    receive: null,
    send: null,
    cardSpendable: null,
    conversionRate: '',
    order: null,
    color: '',
    fee: null,
    feeFormatted: '',
    marketData: {},
    extra: {}
  };

  temp: Symbol = {
    title: 'Not Available',
    symbol: '',
    card: '',
    image: '',
    status: false,
    receive: false,
    send: false,
    cardSpendable: false,
    conversionRate: null,
    order: null,
    color: '',
    fee: null,
    feeFormatted: '',
    marketData: null,
    extra: {}
  };

  toggleChange() {
    this.toggle = !this.toggle;
    this.menu = true;
  }

  searching() {
    let found = false;
    this.symbolsDisplayed = [];
    for (const j in this.symbols) {
      if (this.symbols[j]['title'].toLowerCase().includes(this.searchValue.toLowerCase())
        || this.symbols[j]['symbol'].toLowerCase().includes(this.searchValue.toLowerCase())) {
        this.symbolsDisplayed.push(this.symbols[j]);
        found = true;
      }
    }
    if (!found) {
      this.symbolsDisplayed.push(this.temp);
    }
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,
      Object.assign({}, {class: 'send-modal'}));
  }

  showMenu(p = null) {
    if (p == null) {
      this.menu = !this.menu;
    } else {
      this.menu = !p;
    }
  }

  public readonly transactions: BehaviorSubject<any[]> = new BehaviorSubject([]);

  constructor(private renderer: Renderer2,
              private route: ActivatedRoute,
              public walletService: WalletService,
              public transactionService: TransactionService,
              public symbolService: SymbolService,
              private alertService: AlertService,
              private modalService: BsModalService) {
    this.txUrls = environment.txUrl;
  }

  ngOnInit() {
    const that = this;
    that.wallets = this.walletService.wallets;
    that.symbols = this.symbolService.symbols;
    this.symbolsDisplayed = this.symbols;
    this.selectSymbol(this.route.snapshot.data.defaults.symbol, null);
    this.transactions.next(this.transactionService.transactions);
    this.walletService.walletChange$
      .subscribe(wallets => {


        this.selectedWalletData = wallets
          .find(s => s['symbol'] === that.selectedWallet);
        this.selectedSymbolData = that.symbolService.symbols
          .find(s => s['symbol'] === that.selectedWallet);
        console.log('selectedSymbolData', this.selectedSymbolData);
        that.wallets = wallets;
      });
  }

  buildCss(color) {
    const style = document.createElement('style');
    style.appendChild(document.createTextNode(''));
    document.head.appendChild(style);
    const styleSheet: any = style.sheet;
    styleSheet.insertRule(`
    .dashboard-page:before {
      background-color: ${this.hexToRgba(color, 1)};
    }
    `, 0);
    return style.sheet;
  }

  getValue() {
    return _.find(this.symbols,
      x => x['title'] === this.searchValue || x['symbol'] === this.searchValue);
  }

  showSearchBar() {
    this.SearchBar = !this.SearchBar;
  }

  selectSymbol(s, event) {
    this.menu = true;
    if (event) {
      $('.sidenav ul li').removeClass('selected');
      if ($(event.target)[0].tagName == 'LI') {
        $(event.target).addClass('selected');
      } else {
        $(event.target).parents('li').addClass('selected');
      }
    } else {
      setTimeout(function () {
        $('.sidenav ul li[data-symbol=\'' + s.symbol + '\']').addClass('selected');
        $('ul.scrolling').animate({
          scrollTop: ($('.sidenav ul li[data-symbol=\'' + s.symbol + '\']').offset().top - 125)
        }, 500);
      }, 1000);
    }

    if (s) {
      this.selectedWallet = s.symbol;
      this.selectedSymbolData = s;
      localStorage.setItem('selectedSymbol', s.symbol);
      this.selectedWalletData = this.walletService.wallets
        .find(s1 => s1['symbol'] === s.symbol);
      this.page = 1;
      this.pages = 2;
      this.transactionService.list(s.symbol, 1)
        .subscribe();
      // this.buildCss(this.selectedSymbolData.color);


    }
  }

  collapse(trans) {
    this.selectedTransactionId = this.selectedTransactionId === trans.id ? null : trans.id;
  }

  public onIntersection(event): void {
    console.log('onIntersection');
    if ((this.page >= this.pages || (event && !event.visible))) {
      return;
    }
    if (this.loading) {
      return;
    }
    this.loading = true;
    this.transactionService.list(this.selectedWallet, this.page)
      .subscribe(r => {
          this.loading = false;
          this.pages = r['totalPages'];
          this.page = r['page'] + 1;
          this.transactions.next(this.transactionArray.concat(r['transactions']));
          console.log(r['transactions'], r);
        }
      );
  }

  sendPopup() {
    this.sendModal.openModal();
  }

  receivePopup() {
    this.receiveModal.openModal();
  }

  toggleDefault() {
    this.walletService.toggleDefault(this.selectedWalletData.symbol, !this.selectedWalletData.isDefault)
      .subscribe(r => {
        this.selectedWalletData.isDefault = r.isDefault;
        this.alertService.push('success', `the ${this.selectedWalletData.title} wallet is ${r.isDefault ? 'active' : 'inactive'} `);
      });
  }

  hexToRgba(hex, a) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? `rgba(${parseInt(result[1], 16)},${parseInt(result[2], 16)},${parseInt(result[3], 16)},${a})` : '';

  }


}
