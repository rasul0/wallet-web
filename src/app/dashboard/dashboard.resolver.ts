import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {WalletService} from '../apis/wallet.service';
import {SymbolService} from '../apis/symbol.service';
import {forkJoin, Observable} from 'rxjs';
import {TransactionService} from '../apis/transaction.service';
import {environment} from '../../environments/environment';


@Injectable()
export class DashboardResolver implements Resolve<Event> {

  constructor(
    private walletService: WalletService,
    private symbolService: SymbolService,
    private transactionService: TransactionService) {
  }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Event> | Promise<Event> | Event | any {
    this.walletService.startInterval();
    this.symbolService.startInterval();
    this.transactionService.startInterval();

    const defaultSymbol = localStorage.getItem('selectedSymbol') || environment.defaultSymbol;
    return forkJoin(
      this.walletService.wallet(),
      this.symbolService.symbol()
    ).toPromise()
      .then(x => {
        return {
          wallet: x[0].find(s => s['symbol'] === defaultSymbol),
          symbol: x[1].find(s => s['symbol'] === defaultSymbol),
        };
      });

  }

}
