import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-upper-menu',
  templateUrl: './upper-menu.component.html',
  styleUrls: ['./upper-menu.component.css']
})
export class UpperMenuComponent implements OnInit {
  isCollapsed = true;
  username = '';

  @Input()
  public toggle = false;
  @Output()
  public toggleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private authService: AuthService, private router: Router) {
  }


  ngOnInit() {
    const user = localStorage.getItem('user');
    if (user) {
      this.username = JSON.parse(user)['username'];
    }
  }

  toggleMenu() {
    this.toggle = !this.toggle;
    this.toggleChange.emit(this.toggle);
  }

  logout() {
    this.authService.logout()
      .subscribe(r => {
        localStorage.removeItem('user');
        localStorage.removeItem('accessToken');
        this.router.navigate(['login']);
      }, error1 => {
        localStorage.removeItem('user');
        localStorage.removeItem('accessToken');
        this.router.navigate(['login']);
      });
  }

}
