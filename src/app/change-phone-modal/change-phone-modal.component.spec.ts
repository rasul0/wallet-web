import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePhoneModalComponent } from './change-phone-modal.component';

describe('ChangePhoneModalComponent', () => {
  let component: ChangePhoneModalComponent;
  let fixture: ComponentFixture<ChangePhoneModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePhoneModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePhoneModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
