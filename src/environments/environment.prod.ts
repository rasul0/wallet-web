export const environment = {
  production: true,
  baseUrl: '/',
  interval: (1000 * 60),
  defaultSymbol: 'BTC',
  txUrl: {
    'BTC': 'https://live.blockcypher.com/btc/tx/',
    'LTC': 'https://live.blockcypher.com/ltc/tx/',
    'ETH': '',
    'DASH': '',
  }
};
